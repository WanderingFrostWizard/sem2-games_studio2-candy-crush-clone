﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class NodeManager : MonoBehaviour
{
    [HideInInspector]
    public bool gameInProgress = false;

    // Dimensions of the gameboard
    [Range (3, 6)] public int width;       
    [Range (5, 9)] public int height;

    public GameObject nodePrefab;

    // Choose a nodeHighlight Prefab
    public GameObject nodeHighlight;

    // Actual Instance of the nodeHighlight, that will be used in the gamePlay scene
    private GameObject nodeHighlightIcon;

    // Array to store each of the nodes
    private Node[] nodeArray;

    // Store a reference to the tweener scripts
    public Tweener tweener;

    // Store a reference to the Powerup scripts
    public PowerUP powerup;

    public MenuController menuController;
    public ScoreManager scoreManager;

    // No of blocks that MUST BE IN A ROW to be considered a match
    public int inArow = 3;
    // No of matches required for TNT to generate
    private int matchesForTNT = 0;
    // No of matches required for Lightning to generate
    private int matchesForLightning = 0;

    // Stores the node references to be deleted
    private int[] deleteArray;

    //Score variables
    private int scoreTotal = 0;
    private int highScore;
    private int currentScore;
    private string highScoreKey = "High Score";

    // Movement Variables
    private int moveScoreTotal = 0;
    private int lastMoveScore = 0;
    private int moveLimit = 0; // Maximum number of moves per game
    private int moveCount = 0; // Counts the number of moves we've made (Assignment 2, Angus personal component)

    // Swap Mechanic variables
    private bool nodeSelected = false; // Does the player currently have a node selected to swap?
    private int firstNode, secondNode; // The array reference for the node you select to swap, and the node you swap it with, respectively.

    // Scoreboard variables
    private bool updateDisplayed; // Has an update for the latest move been displayed?
    public Text textMessageBoard;
    bool boardChangeDisplayed;

    public bool boardChange = true;

    // Number of colours used for powerups, including white
    private int numberOfPowerUpColours = 3;
    private int numberOfColours = 0; 

    // Called from NodeManager
    public int NumberOfPowerUpColours
    {
        get 
        {
            return numberOfPowerUpColours;
        }
    }

    public int NumberOfColours
    {
        set 
        {
            numberOfColours = value;
        }
    }

    // Called from NodeManager
    public int MoveLimit 
    {
        set
        {
            moveLimit = value;
        }
    }

    // Called from powerUP
    public int MatchesForTNT
    {
        get 
        {
            return matchesForTNT;
        }
    }

    // Called from powerUP
    public int MatchesForLightning
    {
        get 
        {
            return matchesForLightning;
        }
    }


    // Called from the menuController
    public int ScoreTotal
    {
        get 
        {
            return scoreTotal;
        }
    }


    public void StartGame()
    {
        // Set the number of matches for TNT and Lightning
        matchesForTNT = inArow + 1;
        matchesForLightning = inArow + 2;
        powerup.storeBoardDimensions( width, height );

        gameInProgress = true;
        lastMoveScore = 0;
        scoreTotal = 0;
        scoreManager.startGame();

        // Add the nodeHighlight to the scene. This will be used to show the currently selected node
        nodeHighlightIcon = ( GameObject ) Instantiate( nodeHighlight, new Vector2( 0, 0 ), Quaternion.identity );
        nodeHighlightIcon.SetActive( false );

        // Initialize the gameboard Array with new nodes
        nodeArray = new Node[ width * height ];

        // We create an object of the Tweener class with a maximum list capacity equal to the number of nodes
        tweener.Initialize( width * height );

        // Create a boolean array to show nodes marked for deletion
        deleteArray = new int[ width * height ];

        for ( int i = 0; i < nodeArray.Length; i++ )
        {
            int x = i % width;
            int y = i / width;

            Vector2 position = new Vector2( ( float )( x * 1 ) - ( ( float ) width / 2 ) + 0.5f,
                                            ( float )( y * 1 ) - ( ( float ) height / 2 ) + 1.0f );

            nodeArray[ i ] = ( ( GameObject ) Instantiate( nodePrefab, position, Quaternion.identity ) ).GetComponent< Node >();

            // Generates a new color for each node created
            nodeArray[ i ].Initialize( randomColour(), i );
        }
        textMessageBoard.text = "MOVES COMPLETED: " + moveCount + "/" + moveLimit + "\nCURRENT SCORE: " + scoreTotal + " (+" + lastMoveScore + ") \nHIGH SCORE: " + scoreManager.getHighScore();
    }


    // Unity method called every single frame
    void Update()
    {
        if ( (!tweener.isTweening) && gameInProgress == true )
        {
            if (boardChangeDisplayed != boardChange)
            {
                boardChangeDisplayed = boardChange;
            }

            if (boardChange == true)
            {
                boardChange = checkBoard();
            }
           
            if (boardChange == false && updateDisplayed == false && moveCount > 0)
            {
                if (lastMoveScore > 0)
                {
                    scoreTotal += lastMoveScore;
                }

                textMessageBoard.text = "MOVES COMPLETED: " + moveCount + "/" + moveLimit + "\nCURRENT SCORE: " + scoreTotal + " (+" + lastMoveScore + ") \nHIGH SCORE: " + scoreManager.getHighScore();
                lastMoveScore = 0;
                updateDisplayed = true;

                // Once the player has completed the maximum number of moves, end the game
                if ( moveCount == moveLimit )
                {
                    endGame();
                }

            }
        }

        // If the player clicks the mouse and the tweener function is not currently animating something
        if ( Input.GetMouseButtonDown( 0 ) && !tweener.isTweening )
        {
            // Set the layermask to ONLY check the "Nodes" layer
            LayerMask nodesLayer = LayerMask.GetMask( "Nodes" );

            // Store the players mouseclick as a raycast
            Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );

            RaycastHit2D hit = Physics2D.Raycast( ray.origin, ray.direction, nodesLayer );

            if ( hit.collider != null )
            {
                Node nodeRef = hit.collider.GetComponent< Node >();

                if ( nodeSelected == false )
                {
                    firstNode = nodeRef.ArrayIndex;

                    //If a TNT Powerup is clicked
                    if ( nodeArray[ firstNode ].nodeColor == Node.NodeColors.TNTWhite )
                    {
                        boardChange = true;
                        moveCount++;
                        powerup.TriggerExplosion( firstNode, deleteArray, nodeArray );
                        updateDisplayed = false;
                    }
                    else
                    {
                        //If the node clicked is NOT TNT, set nodeSelected so that the game knows to try to swap with the next node clicked
                        nodeSelected = true;

                        // Node Highlighting Script
                        highlightSelectedNode( firstNode );
                    }
                }
                else if ( nodeSelected == true )
                {
                    secondNode = nodeRef.ArrayIndex;
                    bool isAdjacent = checkAdjacent( firstNode, secondNode );

                    if ( isAdjacent == true )
                    {
                        boardChange = true;
                        moveCount++;
                        nodeSelected = false;
                        updateDisplayed = false;

                        // If the player's first node was a lightning powerup
                        if ( nodeArray[ firstNode ].nodeColor == Node.NodeColors.LightningWhite )
                        {
                            powerup.passToLightningSwap( deleteArray, nodeArray, secondNode );

                            // Once the powerup has been used, it and the second node MUST be marked for deletion and their icons and colours reset
                            powerup.resetUsedPowerup( firstNode, deleteArray, nodeArray );
                            powerup.resetUsedPowerup( secondNode, deleteArray, nodeArray );
                        }
                        else
                        {
                            swapNodes( firstNode, secondNode );
                        }

                        // Hide the nodeHighlightIcon as there is no currently selected node
                        nodeHighlightIcon.SetActive( false );
                    }
                    // If a player has a node highlighted, and clicks to activate a TNT Powerup that is NOT adjacent to the currently highlighted NODE
                    else if ( nodeArray[ secondNode ].nodeColor == Node.NodeColors.TNTWhite )
                    {
                        // Hide the nodeHighlightIcon as it will NOT be needed after the TNT explodes
                        nodeHighlightIcon.SetActive( false );

                        boardChange = true;
                        moveCount++;
                        powerup.TriggerExplosion( secondNode, deleteArray, nodeArray );
                        updateDisplayed = false;
                    }
                    else
                    {
                        // Sets the first Node to the node that has just been clicked
                        firstNode = nodeRef.ArrayIndex;

                        // Node Highlighting Script
                        highlightSelectedNode( firstNode );
                    }
                }

            } // Close IF Hit.collider

            // If the player clicks off screen, clear their currently selected node
            if ( nodeSelected == true && hit.collider == null )
            {
                nodeSelected = false;
                nodeHighlightIcon.SetActive( false );
            }
        }   // END OF MOUSECLICK
    }   // END OF UPDATE()


    // Moves the nodeHighlightIcon over the targetNode, then shows it
    public void highlightSelectedNode( int targetNode )
    {
        nodeHighlightIcon.transform.position = nodeArray[ targetNode ].transform.position;
        nodeHighlightIcon.SetActive( true );
    }


    // Search through the deleteArray and delete any blocks that have been marked for deletion or marked to become powerups
    public bool deleteAll()
    {
        //Place powerups first, before anything can move
        powerup.Create_PowerUp( nodeArray, deleteArray );

        bool deleteAny = false;

        for ( int i = 0; i < nodeArray.Length; i++ )
        {
            if ( deleteArray[ i ] == 1 )
            {
                // If a powerup is deleted, reset it back to a standard node
                if ( nodeArray[ i ].nodeColor < Node.NodeColors.White )
                {
                    powerup.resetUsedPowerup( i, deleteArray, nodeArray );
                }
                // Shift each column down ( also deletes the blocks )
                ShiftColumnDown( i );
                deleteAny = true;
            }
        }

        return deleteAny;
    }


    // Reset the delete array to ALL false
    private void clearDelete()
    {
        for ( int i = 0; i < nodeArray.Length; i++ )
        {
            if ( deleteArray[ i ] > 0 )
            {
                deleteArray[ i ] = 0;
            }
        }
    }

    public int scoreMove(int count, int type) //Type 0 = regular match, type 1 = TNT, type 2 = lightning
    {
        int moveScore = 0;

        switch (type)
        {
            case 0: //Regular move, calculate based on the size of the match
                moveScore = 5 * (int)(Mathf.Pow(2, (count - inArow))); //5 points for a minimal match, doubled for extra node matched
                break;
            case 1: //TNT
                moveScore = 10;
                break;
            case 2: //Lightning
                moveScore = 2 * count;
                break;
            default:
                break;
        }        
        
        if (moveCount > 0)        
            lastMoveScore += moveScore;

        return moveScore;
    }


    // Starts with the index of the node to be deleted
    private void ShiftColumnDown( int deleteIndex )
    {
        // Check the node directly above the node to be deleted
        for ( int y = deleteIndex + width; y < deleteArray.Length; y += width )
        {
            // Find a node that is not going to be deleted
            if ( deleteArray[ y ] != 1 )
            {
                // Change the starting node to false, as their is a node above that can be moved downwards to this position
                deleteArray[ deleteIndex ] = 0;

                // ---=== SHIFT SPRITES LOGIC ===---
                // Check to see if this is a powerup block and if so, swap the sprites on the nodes
                if ( nodeArray[ y ].nodeColor == Node.NodeColors.TNTWhite || nodeArray[y].nodeColor == Node.NodeColors.LightningWhite )
                {
                    // Set the current sprite to be in the lower position
                    nodeArray[ deleteIndex ].AccessSpriteRenderer.sprite = nodeArray[ y ].AccessSpriteRenderer.sprite;
                    powerup.MovePowerUp( nodeArray, deleteIndex, y );

                    // Reset the icon of at the current position to be a circle
                    nodeArray[ y ].ResetIcon();
                }

                // Mark the top node for deletion
                deleteArray[ y ] = 1;

                // Change the color of the selected node to be the color of the node found
                nodeArray[ deleteIndex ].NewColor( nodeArray[ y ].nodeColor );

                // Tweening - Move the node (that isn't going to be deleted) down to the first blank spot
                Vector2 endingPos = nodeArray[ deleteIndex ].transform.position;
                // Sets the position of the node being deleted to the position of the node being moved down
                nodeArray[ deleteIndex ].transform.position = nodeArray[ y ].transform.position;
                tweener.AddTween( nodeArray[ deleteIndex ].transform, endingPos ); /*Then moves the node above down with the tweener method
                * This is all working together with the prior code that replaces the colour of the deleted node with the colour of the next undeleted node above
                * So the deleted node changes colour, teleports instantly up, and moves slowly down to the position it occupied before
                * All giving the optical illusion that bricks are moving around without ever changing the array structure
                */
                break;
            }
        }

        // If the node is marked to be deleted
        if (deleteArray[ deleteIndex ] == 1 )
        {
            nodeArray[ deleteIndex ].NewColor( randomColour() );
            deleteArray[ deleteIndex ] = 0;

            // Tweening - Move the node (that isn't going to be deleted) down to the first blank spot
            Vector2 endingPos = nodeArray[ deleteIndex ].transform.position;
            nodeArray[ deleteIndex ].transform.position = new Vector2( nodeArray[ deleteIndex ].transform.position.x, height );
            tweener.AddTween( nodeArray[ deleteIndex ].transform, endingPos );
        }
    }


    private void swapNodes( int node1, int node2 )
    {
        Node.NodeColors colourTemp;

        //First, we swap the colours of node1 and node2
        colourTemp = nodeArray[ node1 ].nodeColor;

        nodeArray[ node1 ].NewColor( nodeArray[ node2 ].nodeColor );
        nodeArray[ node2 ].NewColor( colourTemp );

        // Check to see if either block is a powerup block and if so, swap the sprites on the nodes
        if ( nodeArray[ node1 ].nodeColor < Node.NodeColors.White || nodeArray[ node2 ].nodeColor < Node.NodeColors.White )
        {
            powerup.MovePowerUp( nodeArray, node1, node2 );
        }

        // Tweening
        // We declare Transform and Vector2 variables for the current state of node1 and node2
        Transform transPos1 = nodeArray[ node1 ].transform;
        Transform transPos2 = nodeArray[ node2 ].transform;

        Vector2 vecPos1 = nodeArray[ node1 ].transform.position;
        Vector2 vecPos2 = nodeArray[ node2 ].transform.position;

        // Move node1 to node2's location, and move node2 to where node1 was
        nodeArray[ node1 ].transform.position = vecPos2;
        nodeArray[ node2 ].transform.position = vecPos1;

        tweener.AddTween( transPos1, vecPos1 );
        tweener.AddTween( transPos2, vecPos2 );
    }


    private bool checkBoard()
    {
        bool boardChange;
        int countH = 0;
        int countV = 0;
        
        int thisMoveScore = 0;

        for ( int i = 0; i < nodeArray.Length; i++ )
        {
            countH = 0;
            countV = 0;
            Node.NodeColors startingColour = nodeArray[ i ].nodeColor;
            if ( deleteArray[ i ] == 0 && startingColour != Node.NodeColors.TNTWhite && startingColour != Node.NodeColors.LightningWhite )
            {
                countH = checkHCC( i, startingColour, false );
                countV = checkVCC( i, startingColour, false );
                if ( countH >= inArow )
                {
                    checkHCC( i, startingColour, true );
                    thisMoveScore += scoreMove(countH,0);
                    
                    // After the player has had their first turn, then add powerups when needed
                    if ( moveCount > 0 )
                    {
                        powerup.making_powerup( countH, deleteArray, i);
                    }

                }
                else if (countV >= inArow)
                {
                    checkVCC( i, startingColour, true );
                    thisMoveScore += scoreMove(countV,0);

                    // After the player has had their first turn, then add powerups when needed
                    if ( moveCount > 0 )
                    {
                        powerup.making_powerup( countV, deleteArray, i);
                    }
                }

                //Check for squares
                if (checkSquare(i, startingColour, 1, 1, false) == true)
                {
                    checkSquare(i, startingColour, 1, 1, true);
                    thisMoveScore += scoreMove(4, 0);
                }
                if (checkSquare(i, startingColour, 1, -1, false) == true)
                {
                    checkSquare(i, startingColour, 1, -1, true);
                    thisMoveScore += scoreMove(4, 0);
                }
                if (checkSquare(i, startingColour, -1, -1, false) == true)
                {
                    checkSquare(i, startingColour, -1, -1, true);
                    thisMoveScore += scoreMove(4, 0);
                }
                if (checkSquare(i, startingColour, -1, 1, false) == true)
                {
                    checkSquare(i, startingColour, -1, 1, true);
                    thisMoveScore += scoreMove(4, 0);
                }
            }
        }

        boardChange = deleteAll();
        clearDelete();

        return boardChange;
    }


    private int checkHCC( int startingPos, Node.NodeColors startingColor, bool delete )
    {
        int countH = 1;

        // The array reference for the left-hand end of this row; first we divide by the width and turn the answer into an integer to find how many rows
        // from the bottom we are, then multiply by the width to get the corresponding array reference.
        int minH = ( startingPos / width ) * width;
        int maxH = minH + width; //The array reference for the right-hand end of this row. Take the left-hand end and add the width

        if( delete == true )
        {
            deleteArray[ startingPos ] = 1; //The starting node will always be the same colour as itself, so to save time, we mark it now
        }

        //Check the horizontal row for adjacent matching colours
        //Start by checking the left hand side, starting 1 to the left of startingPos and continuing until you reach the left-hand end of the row
        for ( int i = startingPos - 1; i >= minH; i-- )
        {
            //If the node currently being checked is the same colour as the one the player clicked, increment the count and mark it for deletion.
            //If it's not, then we end the loop.
            if ( nodeArray[ i ].nodeColor == startingColor && nodeArray[ i ].gameObject.activeSelf )
            {
                countH = marking_Deletion( countH, delete, i );
            }
            else
                break;
        }

        //Now check to the right, starting 1 to the right from startingPos
        for (int i = startingPos + 1; i < maxH; i++)
        {
            //If the node currently being checked is the same colour as the one the player clicked, increment the count and mark it for deletion.
            //If it's not, then we end the loop.
            if ( nodeArray[ i ].nodeColor == startingColor && nodeArray[ i ].gameObject.activeSelf )
            {
                countH = marking_Deletion( countH, delete, i );
            }
            else
                break;
        }

        return countH;
    }

    private int checkVCC( int startingPos, Node.NodeColors startingColor, bool delete )
    {
        int countV = 1;

        if ( delete == true )
        {
            //The starting node will always be the same colour as itself, so to save time, we mark it now
            deleteArray[ startingPos ] = 1;
        }

        // Start 1 position below startingPos and go down
        // Count down by width each time to reach the same column on the next row down
        for ( int i = startingPos - width; i >= 0; i -= width )
        {
            //If the node currently being checked is the same colour as the one the player clicked, increment the count and mark it for deletion.
            //If it's not, then we end the loop.
            if ( nodeArray[ i ].nodeColor == startingColor && nodeArray[ i ].gameObject.activeSelf )
            {
                countV = marking_Deletion( countV, delete, i );
            }
            else
                break;
        }

        //Now go up
        for ( int i = startingPos + width; i < nodeArray.Length; i += width )
        {
            //If the node currently being checked is the same colour as the one the player clicked, increment the count and mark it for deletion.
            //If it's not, then we end the loop.
            if ( nodeArray[ i ].nodeColor == startingColor && nodeArray[ i ].gameObject.activeSelf )
            {
                countV = marking_Deletion( countV, delete, i );
            }
            else
                break;
        }

        return countV;
    }

    private bool checkSquare( int startingPos, Node.NodeColors startingColor, int xMove, int yMove, bool delete )
    {
        int xyPos = startingPos + (yMove * width) + xMove;
        int xPos = startingPos + xMove;
        int yPos = startingPos + (yMove * width);

        //Check that a square in that direction can actually exist without going off the edge of the board
        //First, we check vertically
        if (yPos >= (width * height) || yPos < 0)
            return false;
        //Then horizontally
        
        else if ((xPos % width) < 0 || (xPos % width) >= width || Math.Abs((startingPos % width) - (xPos % width)) > 1)
            return false;
        else if (nodeArray[xyPos].nodeColor != startingColor || nodeArray[yPos].nodeColor != startingColor || nodeArray[xPos].nodeColor != startingColor)
            return false;
        else
        {
            if (delete == true)
            {
                if (deleteArray[startingPos] == 0)
                    deleteArray[startingPos] = 1;
                if (deleteArray[xyPos] == 0)
                    deleteArray[xyPos] = 1;
                if (deleteArray[xPos] == 0)
                    deleteArray[xPos] = 1;
                if (deleteArray[yPos] == 0)
                    deleteArray[yPos] = 1;
            }
            return true;
        }
    }

    private int marking_Deletion( int count, bool delete, int i)
    {
        count++;
        if ( delete == true && deleteArray[ i ] == 0 )
        {
            deleteArray[ i ] = 1;
        }
        return count;
    }


    private bool checkAdjacent(int node1, int node2)
    {
        bool isAdjacent;

        if ( node1 == ( node2 + width ) ) // Is node1 directly above node2?
            isAdjacent = true;
        else if ( node1 == (node2 - width ) ) // Or below?
            isAdjacent = true;
        else if ( node1 == ( node2 + 1 ) && ( node1 % width ) > 0 ) // Is it to the left? Remember to check that there *is* a left of node2
            isAdjacent = true;
        else if ( node1 == ( node2 - 1 ) && ( node2 % width ) > 0 ) // IS it to the right?
            isAdjacent = true;
        else
            isAdjacent = false;

        return isAdjacent;
    }


    public Node.NodeColors randomColour ()
    {
        return ( Node.NodeColors ) UnityEngine.Random.Range( numberOfPowerUpColours, numberOfColours );
    }


    public void endGame()
    {
        // Stops a stack overflow issue if the player quits while the tweener script is running
        tweener.isTweening = false;

        gameInProgress = false;
        powerup.toggleTNTPowerUPS( false );
        powerup.toggleLightningPowerUPS( false );

        moveLimit = 0;
        moveCount = 0;

        scoreManager.endGame(scoreTotal);

        clearDelete();
        boardChange = true;
        destroyAllNodes();

        Destroy( nodeHighlightIcon.gameObject );

        menuController.showResults();
    }


    // Destory ALL GameObjects from the nodeArray to end the game
    private void destroyAllNodes()
    {
        for ( int i = 0; i < nodeArray.Length; i++ )
        {
            Destroy( nodeArray[i].gameObject );
        }
    }
}
