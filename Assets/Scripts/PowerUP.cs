﻿using UnityEngine;
using System.Collections;

public class PowerUP : MonoBehaviour
{
    // References to each of the various powerup Classes
    public NodeManager nodeManager;
    public Lightning lightning;
    public TNT tnt;

    private bool lightningActive = false;
    private bool TNTActive = false;

    // Called from NodeManager - StartGame()
    public void storeBoardDimensions( int width, int height )
    {
        tnt.updateBoardDimensions( width, height );
    }


    // Passes the random colour received from nodeManger function of the same name
    // Used to avoid extra calls to nodeManager
    public Node.NodeColors randomColour ()
    {
        return nodeManager.randomColour ();
    }


    // Set from MenuController
    public void toggleTNTPowerUPS( bool value )
    {
        TNTActive = value;
    }


    // Set from MenuController
    public void toggleLightningPowerUPS( bool value )
    {
        lightningActive = value;
    }


    // Used ONLY to stop an extra reference to the TNT class from nodeManager
    public void TriggerExplosion( int firstNode, int[] deleteArray, Node[] nodeArray )
    {
        tnt.Explosion( firstNode, deleteArray, nodeArray );
        nodeManager.scoreMove(0, 1);
    }


    // Used ONLY to stop an extra reference to the Lightning class from nodeManager
    public void passToLightningSwap( int[] deleteArray, Node[] nodeArray, int secondNode )
    {
        lightning.LightningSwap( deleteArray, nodeArray, secondNode );
    }


    // This method is used by the TNT Powerup to find and trigger any powerups within the blast radius
    public void Check_ForPowerUps( int targetNode, int[] deleteArray, Node[] nodeArray )
    {
        if ( nodeArray[ targetNode ].nodeColor == Node.NodeColors.LightningWhite )
        {
            resetUsedPowerup( targetNode, deleteArray, nodeArray );

            // Select a random node within the range of the board
            int secondNode = Random.Range( 1, nodeArray.Length );

            // While the colur is TNTWhite, LightningWhite or White, select a different node
            while ( nodeArray[ secondNode ].nodeColor <= Node.NodeColors.White )
            {
                secondNode = Random.Range( 1, nodeArray.Length );
            }

            lightning.clickLightningBlock( deleteArray, nodeArray, nodeArray[ secondNode ].nodeColor, false );
        }
        else if ( nodeArray[ targetNode ].nodeColor == Node.NodeColors.TNTWhite )
        {
            tnt.Explosion( targetNode, deleteArray, nodeArray );
            nodeManager.scoreMove(0, 1);
        }
    }

    // Called from NodeManager
    public void Create_PowerUp( Node[] nodeArray, int[] deleteArray )
    {
        //Place powerups first, before anything can move
        for ( int i = 0; i < nodeArray.Length; i++ )
        {
            if ( deleteArray[ i ] == 3 )
            {
                // Set the node where the lightning is to be placed to be LightningWhite
                nodeArray[ i ].NewColor( Node.NodeColors.LightningWhite );
                // Change the Icon to Lightning                
                nodeArray[ i ].ChangeToLightning();
                // Reset the position in the deleteArray
                deleteArray[ i ] = 0;
            }
            else if ( deleteArray[ i ] == 2 )
            {
                // Set the node where the TNT is to be placed to be white
                nodeArray[ i ].NewColor( Node.NodeColors.TNTWhite );
                // Change the Icon to TNT
                nodeArray[ i ].ChangeToTNT();
                // Reset the position in the deleteArray
                deleteArray[ i ] = 0;
            }
        }
    }

    // Called from NodeManager
    public void MovePowerUp( Node[] nodeArray, int node1, int node2 )
    {
        // Swap the sprites on the nodes
        Sprite tempSprite = nodeArray[ node1 ].AccessSpriteRenderer.sprite;

        nodeArray[ node1 ].AccessSpriteRenderer.sprite = nodeArray[ node2 ].AccessSpriteRenderer.sprite;
        nodeArray[ node2 ].AccessSpriteRenderer.sprite = tempSprite;

        // Move the scale of the sprites to correspond to the above changes to nodes / powerups
        Vector2 tempScale = nodeArray[ node1 ].transform.localScale;

        nodeArray[ node1 ].transform.localScale = nodeArray[ node2 ].transform.localScale;
        nodeArray[ node2 ].transform.localScale = tempScale;

        // Move the scale on the nodes collider to correspond to the above changes to the node / powerup
        tempScale = nodeArray[ node1 ].AccessBoxColliderSize;

        nodeArray[ node1 ].AccessBoxColliderSize = nodeArray[ node2 ].AccessBoxColliderSize;
        nodeArray[ node2 ].AccessBoxColliderSize = tempScale;
    }


    // Sets the delete array to allow the creation of powerups on the next update of NodeManger
    // When the update is triggered it runs the deleteAll(), which creates our powerups
    // When the create method is placed here, multiple powerups are created instead of just one
    public void making_powerup( int count, int[] deleteArray, int i )
    {
        // If the player earns a Lightning PowerUP
        if ( lightningActive == true && count >= nodeManager.MatchesForLightning )
        {
            deleteArray[ i ] = 3;
        }
        // If the player earns a TNT PowerUP
        else if ( TNTActive == true && count >= nodeManager.MatchesForTNT )
        {
            deleteArray[ i ] = 2;
        }
    }


    public void resetUsedPowerup( int targetNode, int[] deleteArray, Node[] nodeArray  )
    {
        deleteArray[ targetNode ] = 1;
        nodeArray[ targetNode ].ResetIcon();
        nodeArray[ targetNode ].nodeColor = Node.NodeColors.White;
    }
}
