using UnityEngine;
using System.Collections;

public class TNT : MonoBehaviour
{
    //public NodeManager nodeManager;
    public PowerUP powerup;

    private int TNTExplosionSize = 1;

    // Copy of the board sizes used to trigger explosions
    private int width = 0;
    private int height = 0;


    // Set the starting values for this class
    public void updateBoardDimensions( int width, int height )
    {
        this.width = width;
        this.height = height;
    }


	public void Explosion( int startingPos, int[] deleteArray, Node[] nodeArray )
    {
        // Mark the TNT for deletion and reset it to a standard icon BEFORE continuing
        powerup.resetUsedPowerup( startingPos, deleteArray, nodeArray );
        
        //nodeManager.scoreMove(0, 1);

        Delete_Left( startingPos, deleteArray, nodeArray );

        Delete_Right( startingPos, deleteArray, nodeArray );

        Delete_Up( startingPos, deleteArray, nodeArray );

        Delete_Down( startingPos, deleteArray, nodeArray );

    }

    private bool Check_Left( int startingPos )
    {
        int leftSide = 0;

		for ( int i = 0; i < height; i++ )
	    {
	        if( i != 0 )
	        {
	            leftSide += width;
	        }
            if ( leftSide == startingPos )
	        {
	            return false;
	        }
	    }

        return true;
    }

    private bool Check_Right( int startingPos )
    {
        int rightSide = width - 1;

        for ( int i = 0; i < height; i++ )
        {
            if( i != 0 )
            {
                rightSide += width;
            }

            if ( rightSide == startingPos )
            {
                return false;
            }
        }

        return true;
    }

    private bool Check_Up( int startingPos )
    {
        int maxSize = width * height;

        if ( startingPos < maxSize )
        {
            return true;
        }
        return false;
    }

    private bool Check_Down( int startingPos )
    {
        if ( startingPos >= 0 )
        {
            return true;
        }
        return false;
    }

    private void Delete_Left( int startingPos, int[] deleteArray, Node[] nodeArray )
    {
        for ( int i = 1; i <= TNTExplosionSize; i++ )
        {
			if ( Check_Left( startingPos ) )
			{
				startingPos --;

				check_CurrentLocation( startingPos, deleteArray, nodeArray );
	        }
		}
    }

    private void Delete_Right( int startingPos, int[] deleteArray, Node[] nodeArray )
    {
        for ( int i = 1; i <= TNTExplosionSize; i++ )
        {
			if ( Check_Right( startingPos ) )
		    {
				startingPos ++;

				check_CurrentLocation( startingPos, deleteArray, nodeArray );

			}
        }
    }

    private void Delete_Up( int startingPos, int[] deleteArray, Node[] nodeArray )
    {
        for ( int i = 0; i < TNTExplosionSize; i++ )
        {
			startingPos += width;

			if ( Check_Up( startingPos ) )
			{
				check_CurrentLocation( startingPos, deleteArray, nodeArray );

				Delete_Left( startingPos, deleteArray, nodeArray );
				Delete_Right( startingPos, deleteArray, nodeArray );
            }
        }
    }

    private void Delete_Down( int startingPos, int[] deleteArray, Node[] nodeArray )
    {
        for (int i = 0; i < TNTExplosionSize; i++)
        {
			startingPos -= width;
            if ( Check_Down( startingPos ) )
            {
				check_CurrentLocation( startingPos, deleteArray, nodeArray );

				Delete_Left( startingPos, deleteArray, nodeArray );
				Delete_Right( startingPos, deleteArray, nodeArray );
            }
        }
    }

	private void check_CurrentLocation( int startingPos, int[] deleteArray, Node[] nodeArray )
	{
		if ( nodeArray[ startingPos ].nodeColor == Node.NodeColors.TNTWhite || nodeArray[ startingPos ].nodeColor == Node.NodeColors.LightningWhite )
		{
			powerup.Check_ForPowerUps( startingPos, deleteArray, nodeArray );
		}
		else
		{
			deleteArray[ startingPos ] = 1;
		}
	}
}
