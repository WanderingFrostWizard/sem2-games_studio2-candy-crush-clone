﻿using UnityEngine;
using System.Collections;

public class Node : MonoBehaviour
{
    // Stores the colors the game can use
    public enum NodeColors { TNTWhite, LightningWhite, White, Yellow, Magenta, Red, Green, Blue, Cyan };

    // Sets the unity colors to match the colors above
    public static readonly Color[] NodeColorValues = { Color.white, Color.white, Color.white, Color.yellow, Color.magenta, Color.red, Color.green, Color.blue, Color.cyan };

    private int arrayIndex;

    [HideInInspector]
    public NodeColors nodeColor;

    // Create a reference to the spriteRenderer on each node to allow its sprite to be modified
    private SpriteRenderer spriteRenderer;

    // Possible shapes of each node
    public Sprite Star;
    public Sprite LightningIcon;
    public Sprite TNT;

    // Scaling vectors for changes to the Icons sizes for each of the nodes
    private Vector2 PowerUPScale = new Vector2( 0.6f, 0.6f );
    private Vector2 ResetScale = new Vector2( 0.7f, 0.7f );

    // Scaling vectors for changes to the Collider sizes for each of the nodes    
    private Vector2 PowerUpColliderScale = new Vector2( 1.5f, 1.5f );
    private Vector2 ResetColliderScale = new Vector2( 0.9f, 0.9f );

    // Create a reference to the boxCollider on each node, to allow its size to be modified
    private BoxCollider2D boxCollider;


    public void ChangeToLightning()
    {
        spriteRenderer.sprite = LightningIcon;

        // Shrink the size of the powerup icon to match the size of the other nodes
        transform.localScale = PowerUPScale;

        // Enlarge the size of the collider to offset the change to the localScale
        boxCollider.size = PowerUpColliderScale;
    }


    public void ChangeToTNT()
    {
        spriteRenderer.sprite = TNT;

        // Shrink the size of the powerup icon to match the size of the other nodes
        transform.localScale = PowerUPScale;

        // Enlarge the size of the collider to offset the change to the localScale
        boxCollider.size = PowerUpColliderScale;    
    }


    public void ResetIcon()
    {
        spriteRenderer.sprite = Star;

        // Reset the Size of the node
        transform.localScale = ResetScale;

        // Reset the size of the Collider
        boxCollider.size = ResetColliderScale;
    }

    public Vector2 AccessBoxColliderSize
    {
        get
        {
            return boxCollider.size;
        }

        set
        {
            boxCollider.size = value;
        }
    }

    public SpriteRenderer AccessSpriteRenderer
    {
        get
        {
            return spriteRenderer;
        }

        set
        {
            spriteRenderer = value;
        }
    }


    public int ArrayIndex
    {
        get
        {
            return arrayIndex;
        }
    }


    public void Initialize( NodeColors newColor, int index )
    {
        // Store a reference to the Nodes Collider and its spriteRenderer
        boxCollider = transform.GetComponent<BoxCollider2D>();
        spriteRenderer = GetComponent< SpriteRenderer >();
        NewColor( newColor );
        arrayIndex = index;
    }


    public void NewColor( NodeColors newColor )
    {
        nodeColor = newColor;
        spriteRenderer.color = NodeColorValues[ ( int ) nodeColor ];
    }
}
