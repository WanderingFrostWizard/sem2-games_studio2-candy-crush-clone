﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    // Store a reference to other scripts
    public NodeManager nodeManager;
    public PowerUP powerup;
    public ScoreManager scoreManager;

    // --- === Canvas References === ---
    public GameObject CanvasMainMenu;
    public GameObject CanvasLevelSelect;
    public GameObject CanvasGamePlay;
    public GameObject CanvasShowResults;
    public GameObject CanvasSandboxOptions;

    // Show Results Canvas Points Display
    public Text pointsValue;

    // Sandbox Options Menu Gameobjects
    public Text lblNodesWide;
    public Text lblNodesHigh;
    public Text lblNumberOfColours;
    public Text lblMovesValue;
    public Text lblInARowValue;

    public Slider sliderWidth;
    public Slider sliderHeight;
    public Slider sliderNumberOfColours;
    public Slider sliderMoves;
    public Slider sliderNumberOfMatches;

    public Toggle TNT;
    public Toggle Lightning;


    public void Start()
    {
        // THIS SHOULD NOT BE NEEDED, BUT WAS ADDED AS A SAFE GUARD, WHEN EDITING
        CanvasMainMenu.SetActive( true );
        CanvasLevelSelect.SetActive( false );
        CanvasGamePlay.SetActive( false );
        CanvasShowResults.SetActive( false );
        CanvasSandboxOptions.SetActive( false );

        // Update the Textboxes to show the current values for each Slider bar 
        updateWidth();
        updateHeight();
        updateNoColours();
        updateMoves();
        updateMatches();
    }


    public void gameLevel ( int level )
    {
        // RESET any game values that may have been changed during sandbox mode 
        nodeManager.width = 5;
        nodeManager.height = 8;
        nodeManager.inArow = 3;

        powerup.toggleTNTPowerUPS( false );
        powerup.toggleLightningPowerUPS( false );

        //nodeManager.setHighScoreKey("High Score" + level);
        scoreManager.setLevel(level);

        switch ( level )
        {
            case 1: 
            {
                setEasy();
                break;
            }
            case 2: 
            {
                setHard();
                break;
            }
            case 3: 
            {
                setEasy();
                powerup.toggleTNTPowerUPS( true );
                break;
            }
            case 4: 
            {
                setHard();
                powerup.toggleTNTPowerUPS( true );
                break;
            }
            case 5:  
            {
                setEasy();
                powerup.toggleTNTPowerUPS( true );
                powerup.toggleLightningPowerUPS( true );
                break;
            }
            case 6: 
            {
                setHard();
                powerup.toggleTNTPowerUPS( true );
                powerup.toggleLightningPowerUPS( true );
                break;
            }
        }
        switchToGameCanvas();
        nodeManager.StartGame();
    }


    /* The total Number of colours is based upon an array stored under the node script. The first few colours are used to reference powerups
     * and SHOULD NOT BE GENERATED ANY other way. */
    public void setEasy ()
    {
        nodeManager.MoveLimit = 35;
        nodeManager.NumberOfColours = 4 + nodeManager.NumberOfPowerUpColours;
    }

    public void setHard ()
    {
        nodeManager.MoveLimit = 25;
        nodeManager.NumberOfColours = 6 + nodeManager.NumberOfPowerUpColours;
    }

    // Called when starting a game
    private void switchToGameCanvas()
    {
        CanvasLevelSelect.SetActive( false );
        CanvasGamePlay.SetActive( true );
    }

    // Called from the Main Menu
    public void quitApplication ()
    {
        Debug.Log ( " Quit Applcation " );
        Application.Quit();
    }

    // Executed from CanvasShowResults
    public void returnToMenu()
    {
        CanvasShowResults.SetActive( false );
        CanvasLevelSelect.SetActive( true );
    }

    // Called from the nodeManager at the end of each game, or when the player presses the endGame Button
    public void showResults()
    {
        CanvasGamePlay.SetActive( false );
        CanvasShowResults.SetActive( true );
        pointsValue.text = nodeManager.ScoreTotal.ToString();
    }

    /* ############################################# SANDBOX FUNCTIONS ################################################ 
     *   PLEASE NOTE THIS WAS ADDED HERE INSTEAD OF ITS OWN SCRIPT TO MINIMIZE THE NUMBER OF GAMEOBJECTS, SCRIPTS ETC 
     *   THAT NEED TO BE LINKED FOR IT TO FUNCTION CORRECTLY
     * ##############################################################################################################*/


    public void updateWidth()
    {
        lblNodesWide.text = "Nodes Wide: " + sliderWidth.value;
    }

    public void updateHeight()
    {
        lblNodesHigh.text = "Nodes High: " + sliderHeight.value;
    }

    public void updateNoColours()
    {
        lblNumberOfColours.text = "Colours to Randomize: " + sliderNumberOfColours.value;
    }

    public void updateMoves()
    {
        lblMovesValue.text = "Number of Moves: " + sliderMoves.value;
    }

    public void updateMatches()
    {
        lblInARowValue.text = "Nodes required for a Match: " + sliderNumberOfMatches.value;
    }

    public void sandboxOptionsMenu()
    {
        CanvasLevelSelect.SetActive( false );
        CanvasSandboxOptions.SetActive( true );
    }

    public void startSandboxLevel()
    {   
        // Set the values for each of the Slider bars 
        nodeManager.width = (int)sliderWidth.value;
        nodeManager.height = (int)sliderHeight.value;
        nodeManager.NumberOfColours = (int)sliderNumberOfColours.value + nodeManager.NumberOfPowerUpColours;
        nodeManager.MoveLimit = (int)sliderMoves.value;
        nodeManager.inArow = (int)sliderNumberOfMatches.value;
        
        // Set the selected powerups
        powerup.toggleTNTPowerUPS( TNT.isOn );
        powerup.toggleLightningPowerUPS( Lightning.isOn );

        // Set the Correct scene ready to start the game
        CanvasSandboxOptions.SetActive( false );
        CanvasGamePlay.SetActive( true );
        nodeManager.StartGame();
    }
}
