﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour {

    //Other-script references
    public NodeManager nodeManager;

    //Score variables
    private int highScore;
    private int currentScore;
    private int level;
    private string highScoreKeyBase = "High Score";
    private string highScoreKey;

    // Use this for initialization
    public void startGame()
    {
        this.setHighScoreKey(highScoreKeyBase + level);
        setHighScore(PlayerPrefs.GetInt(getHighScoreKey(), 0));
    }

    public void endGame(int score)
    {
        if (getHighScore() < score)
        {
            PlayerPrefs.SetInt(getHighScoreKey(), score);
            PlayerPrefs.Save();
        }

        setLevel(0);
    }

    public void setHighScore(int i)
    {
        highScore = i;
    }

    public int getHighScore()
    {
        return highScore;
    }

    public void setHighScoreKey(string i)
    {
        highScoreKey = i;
    }

    public void setHighScoreLevel(int i)
    {
        this.setHighScoreKey(this.highScoreKeyBase + i);
    }

    public string getHighScoreKey()
    {
        return highScoreKey;
    }

    public void setLevel (int i)
    {
        this.level = i;
    }

    public int getLevel()
    {
        return this.level;
    }

    public void clearHighScore(int level)
    {
        PlayerPrefs.SetInt(getHighScoreKey() + level, 0);
        PlayerPrefs.Save();
    }

    public void clearAllHighScores()
    {
        PlayerPrefs.SetInt(getHighScoreKey(), 0);
        PlayerPrefs.Save();

        for (int i = 1; i <= 6; i++)
        {
            PlayerPrefs.SetInt(getHighScoreKey() + i, 0);
            PlayerPrefs.Save();
        }
    }


}
