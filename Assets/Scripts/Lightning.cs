﻿using UnityEngine;
using System.Collections;

public class Lightning : MonoBehaviour 
{
    public NodeManager nodeManager;
    public PowerUP powerup;

    public void clickLightningBlock( int[] deleteArray, Node[] nodeArray, Node.NodeColors targetColor, bool generatesTNT )
    {
        int nodesDeleted = 0;

        // Search through the nodeArray for the required color and set it to be deleted within the deleteArray
        for ( int i = 0; i < nodeArray.Length; i++ )
        {
            if ( nodeArray[ i ].nodeColor == targetColor )
            {
                // If the lightning block was swapped with a standard node, then just delete the block
                if ( generatesTNT == false )
                {
                    deleteArray[ i ] = 1;
                    nodesDeleted++;
                }
                // If the lightning block was swapped with a TNT node, then change each node of the chosen color to a TNT block
                else if ( generatesTNT == true )
                {
                    deleteArray[ i ] = 2;
                }
            }
        }

        nodeManager.scoreMove(nodesDeleted, 2);
    }


    public void LightningSwap( int[] deleteArray, Node[] nodeArray, int secondNode )
    {
        // If Player swaps a lightning and a TNT, Trigger the lightning with the generatesTNT flag set to true;
        if  ( nodeArray[ secondNode ].nodeColor == Node.NodeColors.TNTWhite )
        {
            clickLightningBlock( deleteArray, nodeArray, powerup.randomColour(), true );
        }
        // If player swaps two lightning - Trigger all powerups on the board, then delete each remaining node
        else if ( nodeArray[ secondNode ].nodeColor == Node.NodeColors.LightningWhite )
        {
            markAllForDelete( deleteArray, nodeArray );
        }
        // If the player swaps a Lightning with a standard node - Trigger lightning, with the generatesTNT flag set to false;
        else 
        {
            clickLightningBlock( deleteArray, nodeArray, nodeArray[ secondNode ].nodeColor, false );
        }
    }


    private void markAllForDelete( int[] deleteArray, Node[] nodeArray )
    {
        for ( int i = 0; i < nodeArray.Length; i++ )
        {
            deleteArray[ i ] = 1;
            nodeManager.scoreMove(1, 2);
        }
    }
}