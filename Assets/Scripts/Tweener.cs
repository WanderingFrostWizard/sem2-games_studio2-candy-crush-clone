﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

//Animator
public class Tweener : MonoBehaviour
{
    List< Tween > tweenList; //For cases where we want to animate many different objects simultaneously
    public bool isTweening;

    private bool IsTweening
    {
        get
        {
            return isTweening;
        }
    }


    public void Initialize(int listCapacity)
    {
        tweenList = new List< Tween >( listCapacity );
        isTweening = false;
    }


    public void AddTween(Transform objectTrans, Vector2 endingPosition) //Same arguments as the tween constructors
    {
        tweenList.Add( new Tween( objectTrans, endingPosition ) ); //Add() is a method of the List class, allowing you to add a new entry.
        isTweening = true;
    }


    void Update()
    {
        // If there are no active tweens, might as well stop here rather than bothering to check every single entry in the list
        if ( !isTweening )
            return; 
        if ( tweenList.Count == 0 )
        {
            isTweening = false;
            return;
        }
        //Counting backwards here - if we delete an item in the list at any point, all the items above it on the list shift down one space. 
        //If we count up and delete the List[i], then List[i+1] becomes List[i] and List[i+2] becomes List[i+1]
        //Then we move on to count List[i+1], missing the new List[i]
        //Counting down avoids this - any and all list items that move down have already been checked
        for ( int i = tweenList.Count - 1; i >= 0; i-- )
        {
            bool activeTween = tweenList[ i ].UpdateTween( Time.deltaTime );
            if ( !activeTween ) //That is, if the boolean value activeTween = false
            {
                // RemoveAt is another List method, removing the specified entry and moving down all list items above it
                tweenList.RemoveAt( i ); 
            }
        }
    }


    //An animation
    private class Tween //This is an "inner-class" - the only class that can access this class is the one it's nested inside
    {
        private const float tweenDuration = 0.5f; //How long do we want the movement to take? Making it a constant means it will always take the same amount of time, namely 0.5 seconds
        private Transform objTransform;
        private Vector2 startPos; //Where our object starts off
        private Vector2 endPos;  //Where our object finishes up
        private float currentTime; //How long has it been since we started this tween?


        public Tween( Transform newObjTransform, Vector2 newEndPos ) //So we enter the object's current position and its destination.
        {
            objTransform = newObjTransform;
            startPos = newObjTransform.position;
            endPos = newEndPos;
            currentTime = 0.0f;
        }


        public bool UpdateTween( float deltaTime )
        {
            // Increment the current time by the delta time
            // deltaTime is the time between now and the last time deltaTime was called
            currentTime += deltaTime; 

            // If we haven't been moving for the full specified time, keep moving
            if ( currentTime <= tweenDuration ) 
            {
                Vector2 currentPos = Vector2.zero;

                //We won't be using x-movement in this assignment, but might in other projects
                currentPos.x = Mathf.Lerp( startPos.x, endPos.x, currentTime / tweenDuration ); 
                //Lerp = Linear interpolation
                currentPos.y = Mathf.Lerp( startPos.y, endPos.y, currentTime / tweenDuration );

                objTransform.position = currentPos;
                return true;
            }
            // Once the tweenDuration is complete finish the function
            else 
            {
                objTransform.position = endPos;
                return false;
            }
        }
    }    
}